/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_EXECUTEACTIONUTILS
#define INCLUDED_BUILDBOXWORKER_EXECUTEACTIONUTILS

#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>

namespace buildboxworker {

struct ExecuteActionUtils {
    typedef build::bazel::remote::execution::v2::ExecutedActionMetadata
        ExecutedActionMetadata;
    static void updateExecuteActionMetadataWorkerProperties(
        ExecutedActionMetadata *metadata, const std::string &botID);
};
} // namespace buildboxworker

#endif
