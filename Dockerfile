FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

ARG EXTRA_CMAKE_FLAGS=
ARG BUILD_TESTS=OFF

ENV BUILDBOX_COMMON_SOURCE_ROOT=/buildbox-common
ENV CMAKE_OPTS="DCMAKE_BUILD_TYPE=DEBUG"

COPY . /buildbox-worker

RUN cd /buildbox-worker && mkdir -p build && cd build && \
    cmake ${CMAKE_OPTS} -DBUILD_TESTING=${BUILD_TESTS} "${EXTRA_CMAKE_FLAGS}" .. && make -j$(nproc)

ENV PATH "/buildbox-worker/build:$PATH"
